###############################################################################
LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)
LOCAL_MODULE := TunerSettings
LOCAL_MODULE_CLASS := APPS
LOCAL_MODULE_TAGS := optional
LOCAL_BUILT_MODULE_STEM := package.apk
LOCAL_MODULE_SUFFIX := $(COMMON_ANDROID_PACKAGE_SUFFIX)
LOCAL_PRIVILEGED_MODULE := true
LOCAL_CERTIFICATE := platform

ifneq ($(filter atv box, $(strip $(TARGET_BOARD_PLATFORM_PRODUCT))), )
    LOCAL_SRC_FILES := TunerSettings-tv-release.apk
    LOCAL_MODULE_STEM := TunerSettings-tv.apk
else
    LOCAL_SRC_FILES := TunerSettings-tablet-release.apk
    LOCAL_MODULE_STEM := TunerSettings-phone.apk
endif

include $(BUILD_PREBUILT)
